package com.jtof;

import java.util.Random;

import org.junit.Test;

/**
 * 
 * @author atlas
 * @date 2012-12-5
 */
public class DefaultTableFormatterTest {
	@Test
	public void testBasic() {
		DefaultTable dt = new DefaultTable();
		dt.setTitle("Resource  Status");
		dt.setHeaders(new String[] { "Id", "Name", "Node", "Status" });
		dt.addRow(getRows(0));
		dt.addRow(getRows(1));
		dt.addRow(getRows(2));
		DefaultTableFormatter dtf = new DefaultTableFormatter(100, 2);
		System.out.println(dtf.format(dt));
	}

	@Test
	public void testLongCell() {
		DefaultTable dt = new DefaultTable();
		dt.setTitle("Resource  Status");
		dt.setHeaders(new String[] { "Id", "Name", "Node", "Status" });
		dt.addRow(getRows(0));
		dt.addRow(getRowsRandom(1));
		dt.addRow(getRowsRandom(2));
		dt.addRow(getRowsRandom(2));
		DefaultTableFormatter dtf = new DefaultTableFormatter(100, 2);
		System.out.println(dtf.format(dt));
	}

	@Test
	public void testLongTitl() {
		DefaultTable dt = new DefaultTable();
		dt.setTitle("Longongongongongongongongongongongongongongongongongongongongongongongongongongongongongongong title");
		dt.setHeaders(new String[] { "Id", "Name", "Node", "Status" });
		dt.addRow(getRows(0));
		dt.addRow(getRows(1));
		dt.addRow(getRows(2));
		DefaultTableFormatter dtf = new DefaultTableFormatter(100, 2);
		System.out.println(dtf.format(dt));
	}

	@Test
	public void testLongHeaders() {
		DefaultTable dt = new DefaultTable();
		dt.setTitle("A title");
		dt.setHeaders(new String[] { "LLLLLLLLLLLLLLLLLLLLLLLLLLLlongId",
				"Name", "Node", "Status" });
		dt.addRow(getRows(0));
		dt.addRow(getRows(1));
		dt.addRow(getRows(2));
		DefaultTableFormatter dtf = new DefaultTableFormatter(100, 2);
		System.out.println(dtf.format(dt));
	}

	@Test
	public void testShortWholeWidth() {
		DefaultTable dt = new DefaultTable();
		dt.setTitle("Resource  Status");
		dt.setHeaders(new String[] { "Id", "Name", "Node", "Status" });
		// dt.addRow(getRows(0));
		// dt.addRow(getRowsRandom(1));
		// dt.addRow(getRowsRandom(3));
		// dt.addRow(getRowsRandom(4));
		DefaultTableFormatter dtf = new DefaultTableFormatter(50, 2);
		System.out.println(dtf.format(dt));
	}

	@Test
	public void testSort1() {
		DefaultTable dt = new DefaultTable();
		dt.setTitle("A sorted title");
		dt.setHeaders(new String[] { "Id", "Name" });
		dt.addRow(new Object[] { 3, "Node1" });
		dt.addRow(new Object[] { 1, "Node1" });
		dt.addRow(new Object[] { 4, "Node1" });
		dt.addRow(new Object[] { 2, "Node1" });
		DefaultTableFormatter dtf = new DefaultTableFormatter(100, 2);
		System.out.println(dtf.format(dt));
	}

	@Test
	public void testSort2() {
		DefaultTable dt = new DefaultTable();
		dt.setTitle("A sorted title");
		dt.setHeaders(new String[] { "Id", "Name" });
		dt.addRow(new Object[] { 3, "Node4" });
		dt.addRow(new Object[] { 1, "Node3" });
		dt.addRow(new Object[] { 3, "Node2" });
		dt.addRow(new Object[] { 1, "Node1" });
		DefaultTableFormatter dtf = new DefaultTableFormatter(100, 2);
		System.out.println(dtf.format(dt));
	}

	@Test
	public void testSort3() {
		DefaultTable dt = new DefaultTable();
		dt.setTitle("A sorted title");
		dt.setHeaders(new String[] { "Id", "Name" });
		dt.addRow(new Object[] { 3, "Node1" });
		dt.addRow(new Object[] { 1, "Node1" });
		dt.addRow(new Object[] { 4, "Node1" });
		dt.addRow(new Object[] { 2, "Node1" });
		DefaultTableFormatter dtf = new DefaultTableFormatter(100, 2);
		System.out.println(dtf.format(dt));
	}

	@Test
	public void testIndent() {
		DefaultTable dt = new DefaultTable();
		dt.setTitle("A sorted title");
		dt.setHeaders(new String[] { "Id", "Name" });
		dt.addRow(new Object[] { 3, "Node1" });
		dt.addRow(new Object[] { 1, "Node1" });
		dt.addRow(new Object[] { 4, "Node1" });
		dt.addRow(new Object[] { 2, "Node1" });
		DefaultTableFormatter dtf = new DefaultTableFormatter(100, 2);
		dtf.setIndent("  *  ");
		System.out.println(dtf.format(dt));
	}

	public static String[] getRows(int row) {
		return new String[] { "id" + row, "Node" + row, row + "sss", "Stopped" };
	}

	static Random r = new Random();

	public static String[] getRowsRandom(int row) {
		return new String[] { "id" + row, Util.randomStr(10 * row) + row,
				row + "sss" + r.nextLong(), "Stopped" };
	}
}