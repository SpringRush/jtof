package com.jtof;

import java.util.Arrays;
import java.util.Random;

/**
 * 
 * @author atlas
 * @date 2013-10-17
 */
public class Util {

	public static String repeat(char ch, int count) {
		if (count <= 0) {
			return "";
		}
		char[] data = new char[count];
		Arrays.fill(data, 0, data.length, ch);
		return new String(data);
	}

	public static boolean isEmpty(String str) {
		if (str == null || "".equals(str.trim()))
			return true;
		return false;
	}

	public static int sum(int[] d) {
		int sum = 0;
		for (int d0 : d) {
			sum += d0;
		}
		return sum;
	}

	/**
	 * 生成指定长度由小写字母和数字组成的随机字符串
	 * 
	 * @param length
	 * @return
	 */
	public static String randomStr(int length) {
		String result = "";
		for (int i = 0; i < length; i++) {
			// 生成下标的随机数
			Random random = new Random();
			int index = random.nextInt(chars.length);
			char tempChar = chars[index];
			result += tempChar;
		}
		return result;
	}

	private static final char[] chars = { 'a', 'b', 'c', 'd', 'e', 'f', 'g',
			'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
			'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6',
			'7', '8', '9' };

}
