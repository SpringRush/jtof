package com.jtof;

import static java.lang.System.getProperty;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author atlas
 * @date 2012-12-5
 */
public class DefaultTableFormatter implements TableFormatter {
	public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");
	public static final String LINE_SEPARATOR = getProperty("line.separator");

	private List<CellFormatter> formatters = new ArrayList<CellFormatter>(0);

	private static final CellFormatter DEFAULT_CELLFORMATTER = new CellFormatter() {
		public String format(Object cell) {
			return cell == null ? "" : cell.toString();
		}

		public boolean accepts(Object cell) {
			return true;
		}
	};
	/**
	 * 全表总宽度
	 */
	private final int totalWidth;
	/**
	 * 每列间隔宽度
	 */
	private final int columnSeparatorWidth;

	/**
	 * 填充标题的字符
	 */
	private char titleFillChar = '=';
	/**
	 * 分割表头和表体的字符
	 */
	private char headerSplitChar = '-';
	/**
	 * 整个表格的缩进
	 */
	private String indent = "";
	/**
	 * 排序规则：按第一列从小到大排，相同时按第二列从小到达排...
	 */
	private boolean sort = false;

	public DefaultTableFormatter() {
		this(120, 2);
	}

	public DefaultTableFormatter(int overallWidth, int columnSeparatorWidth) {
		this.totalWidth = overallWidth;
		this.columnSeparatorWidth = columnSeparatorWidth;
	}

	public void addCellFormatter(CellFormatter... formatters) {
		this.formatters.addAll(Arrays.asList(formatters));
	}

	private CellFormatter getCellFormatter(Object data) {
		for (CellFormatter cf : formatters) {
			if (cf.accepts(data)) {
				return cf;
			}
		}
		return DEFAULT_CELLFORMATTER;
	}

	private void convert(List<Row> rows) {
		for (Row row : rows) {
			for (int i = 0; i < row.get().length; i++) {
				Object cell = row.get(i);
				cell = getCellFormatter(cell).format(cell);
				row.set(i, cell);
			}
		}
	}

	protected void sort(List<Row> rows) {
		Collections.sort(rows);
	}

	public void setIndent(String indent) {
		this.indent = indent;
	}

	public void setSort(boolean sort) {
		this.sort = sort;
	}

	protected void preProcess(List<Row> rows) {
		if (sort) {
			sort(rows);
		}
		convert(rows);
	}

	@Override
	public String format(Table table) {
		StringBuilder buffer = new StringBuilder();
		try {
			format(table, buffer);
		} catch (IOException e) {
			// will never happend
		}
		return buffer.toString();
	}

	@Override
	public void format(Table table, Appendable buffer) throws IOException {
		List<Row> rows = new ArrayList<Row>(table.getRows());
		preProcess(rows);

		String title = table.getTitle();
		// 列数
		int cols = table.getHeaders().length;
		// longestOfColumns[i]表示第i列的内容的最大长度
		int[] longestOfColumns = new int[cols];

		int[] headersWidth = new int[cols];
		for (int i = 0; i < table.getHeaders().length; i++) {
			String headeri = table.getHeaders()[i];
			if (Util.isEmpty(headeri)) {
				throw new IllegalArgumentException("Empty header " + i);
			}
			headersWidth[i] = table.getHeaders()[i].length();
		}
		for (int i = 0; i < rows.size(); i++) {
			Row row = rows.get(i);
			for (int j = 0; j < row.length(); j++) {
				if (j >= longestOfColumns.length) {
					throw new IllegalArgumentException("column is "
							+ longestOfColumns.length + ",but row " + i
							+ " has " + row.length() + " columns");
				}
				int len = row.getLength(j);
				headersWidth[j] = table.getHeaders()[j] != null ? table
						.getHeaders()[j].length() : 0;
				len = Math.max(len, headersWidth[j]);
				longestOfColumns[j] = Math.max(longestOfColumns[j], len);
			}
		}
		int allHeaderWidth = Util.sum(headersWidth);
		// 内容总宽度
		int totalContentWidth = Util.sum(longestOfColumns);
		// 分割符总宽度
		int totalSeparatorWidth = (cols - 1) * columnSeparatorWidth;
		// 内容不换行表格的总宽度
		int totalCellWidth = totalContentWidth + totalSeparatorWidth;

		// 保证标题（加前后一个空格）只有一行
		int overallWidth = Math.max(this.totalWidth, title.length() + 2);
		// 保证标题只有一行
		overallWidth = Math.max(overallWidth, totalSeparatorWidth
				+ allHeaderWidth);

		// 不换行是否足以容纳所有列
		boolean enough = totalCellWidth <= overallWidth;
		// 每个列内容的平均长度
		int cellLen = (overallWidth - totalSeparatorWidth) / cols;

		// 最终计算出来的每一列内容的最大宽度
		int[] widthOfColumns = new int[cols];
		// 最终计算出来的表格的总宽度
		int totalLength = totalSeparatorWidth;
		for (int i = 0; i < widthOfColumns.length; i++) {
			// if enough,那么不用换行，每列该多宽就多宽，否则取与平均值相比最少的一个值
			widthOfColumns[i] = enough ? longestOfColumns[i] : Math.min(
					cellLen, longestOfColumns[i]);
			// 保证标题只有一行
			widthOfColumns[i] = Math.max(widthOfColumns[i], headersWidth[i]);
			totalLength += widthOfColumns[i];
		}
		// 组装
		// 组装标题
		int padHeader = (totalLength - title.length() + 1) / 2 - 2;
		buffer.append(indent);
		buffer.append(Util.repeat(titleFillChar, padHeader));
		buffer.append(' ');
		buffer.append(title);
		buffer.append(' ');
		buffer.append(Util.repeat(titleFillChar, padHeader));
		buffer.append(LINE_SEPARATOR);
		// 组装表头
		addRow(buffer, table.getHeaders(), widthOfColumns);
		// 组装表头与表体的分割线
		buffer.append(indent);
		for (int i = 0; i < cols; i++) {
			buffer.append(Util.repeat(headerSplitChar, widthOfColumns[i]));
			buffer.append(Util.repeat(' ', this.columnSeparatorWidth));
		}
		buffer.append(LINE_SEPARATOR);

		// 组装表体
		for (Row row : rows) {
			addRow(buffer, row.get(), widthOfColumns);
		}
	}

	public void addRow(Appendable buffer, Object[] row, int[] wideOfColumns)
			throws IOException {
		buffer.append(indent);
		String[] nextRow = new String[row.length];
		boolean repeart = true;
		while (repeart) {
			repeart = false;
			nextRow = new String[row.length];
			for (int i = 0; i < row.length; i++) {
				String td = row[i] != null ? row[i].toString() : "";
				int w = wideOfColumns[i];
				int pad = w - (td != null ? td.length() : 0);
				if (td == null) {
					buffer.append(Util.repeat(' ', pad));
				} else {
					if (pad >= 0) {
						buffer.append(td);
						buffer.append(Util.repeat(' ', pad));
					} else if (pad < 0) {
						repeart = true;
						buffer.append(td.subSequence(0, w));
						nextRow[i] = td.substring(w);
					}
				}
				if (i < row.length - 1)
					buffer.append(Util.repeat(' ', this.columnSeparatorWidth));
			}
			buffer.append(LINE_SEPARATOR);
			if (repeart) {
				row = nextRow;
			}
		}
	}

}
