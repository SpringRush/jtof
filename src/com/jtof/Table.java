package com.jtof;

import java.util.List;

/**
 * 
 * @author atlas
 * @date 2012-12-5
 */
public interface Table {
	String getTitle();

	String[] getHeaders();

	List<Row> getRows();

}
