package com.jtof;

import java.io.IOException;

/**
 * 
 * @author atlas
 * @date 2012-12-5
 */
public interface TableFormatter {
	/**
	 * format this table to writer
	 * 
	 * @param table
	 * @param writer
	 * @throws IOException
	 */
	void format(Table table, Appendable writer) throws IOException;

	String format(Table table);

}
